## Project TODOs

### Important
- [ ] Customize template to better match my usage.
- [ ] Make comments available via Ajax

### Extra

- [ ] Add external rewrite rules file. (using ring-rewrite)
- [ ] Add page caching
- [ ] Try using an asset pipeline (such as optimus)